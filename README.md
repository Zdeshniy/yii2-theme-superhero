Yii2  Superhero Theme 
======================
Theme for Yii2 Web Applicaiton

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist kongoon/yii2-theme-superhero "*"
```

or add

```
"kongoon/yii2-theme-superhero": "*"
```

to the require section of your `composer.json` file.


Usage
-----
Open your layout e.g. views/layouts/main.php and add
```
use kongoon\theme\superhero;

superhero\SuperheroAsset::register($this);
```