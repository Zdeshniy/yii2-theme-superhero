<?php
/*
 * 2014-12-22
 * @author Programmer Thailand <contact@programmerthailand.com>
 * http://bootswatch.com/superhero/
 */
namespace kongoon\theme\superhero;

use yii\web\AssetBundle;
class SuperheroAsset extends AssetBundle
{
    public $sourcePath='@vendor/kongoon/yii2-theme-superhero/assets';
    public $baseUrl = '@web';
    
    public $css=[
        'css/bootstrap.css',
        'css/style.css',
    ];
    
    public $js=[
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'yii\bootstrap\BootstrapPluginAsset',
    ];
    
    public function init() {
        parent::init();
    }
}

